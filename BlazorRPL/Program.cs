using System.Security.Claims;
using BlazorRPL.Data;
using BlazorRPL.Infrastructure;
using BlazorRPL.Mappings;
using BlazorRPL.Pages;
using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

var builder = WebApplication.CreateBuilder(args);
SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddSingleton<WeatherForecastService>();
builder.Services.AddSingleton<CalendarGenerationService>();
builder.Services.AddScoped<UserState>();

builder.Services.AddOptions();
builder.Services.AddAuthenticationCore();
builder.Services.AddScoped<AuthenticationStateProvider, TokenAuthenticationStateProvider>();
builder.Services.AddScoped<ILocalStorageService, LocalStorageService>();

var connectionMongoString = builder.Configuration.GetConnectionString("MongoConnection");
Config.ConnectionString = connectionMongoString;
Console.WriteLine(connectionMongoString);
builder.Services.AddScoped(_ => new MongoClient(connectionMongoString));
var conventionPack = new ConventionPack
{
    new CamelCaseElementNameConvention()
};
ConventionRegistry.Register("camelCase", conventionPack, t => true);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();

public static class Config
{
    public static string ConnectionString { get; set; }
    public static string UserName { get; set; } = "";
    public static SecurityToken? SecurityToken { get; set; }
    public static Dictionary<ObjectId, string> TeamDictionary { get; set; } = new Dictionary<ObjectId, string>()
    {
        [ObjectId.Parse("63a0b011e828aa5288c7a4d0")] = "�����",
        [ObjectId.Parse("63a0d160e828aa5288c7a4ef")] = "�����",
        [ObjectId.Parse("63a0d160e828aa5288c7a4f0")] = "������",
        [ObjectId.Parse("63a0d160e828aa5288c7a4f1")] = "���������",
        [ObjectId.Parse("63a0d160e828aa5288c7a4f2")] = "������ �������",
        [ObjectId.Parse("63a0d160e828aa5288c7a4f3")] = "���������",
        [ObjectId.Parse("63a0d160e828aa5288c7a4f4")] = "��������",
        [ObjectId.Parse("63a0d160e828aa5288c7a4f5")] = "���� ��",
        [ObjectId.Parse("63a0d160e828aa5288c7a4f6")] = "������",
        [ObjectId.Parse("63a0d160e828aa5288c7a4f7")] = "����",
        [ObjectId.Parse("63a0d160e828aa5288c7a4f8")] = "�������",
        [ObjectId.Parse("63a0d160e828aa5288c7a4f9")] = "�������",
        [ObjectId.Parse("63a0d160e828aa5288c7a4fa")] = "����",
        [ObjectId.Parse("63a0d160e828aa5288c7a4fb")] = "�����",
        [ObjectId.Parse("63a0d160e828aa5288c7a4fc")] = "�����",
        [ObjectId.Parse("63a0d160e828aa5288c7a4fd")] = "����",
    };
}

public class TokenAuthenticationStateProvider : AuthenticationStateProvider
{
    private readonly ILocalStorageService _localStorageService;
    private readonly IMongoDatabase _dataStore;
    [Inject] public MongoClient Db { get; set; }
    public TokenAuthenticationStateProvider(ILocalStorageService localStorageService)
    {
        _localStorageService = localStorageService;
        Db = new MongoClient(Config.ConnectionString);
        _dataStore = Db.GetDatabase("premierleague");
    }
    private async Task<Person[]> GetPersons()
    {
        var collection = _dataStore.GetCollection<Person>("person");
        using var cursors = await collection.FindAsync(new BsonDocument());
        return cursors.ToList().Where(x => x != null).ToArray();
    }
    private async Task<Player[]> GetPlayers()
    {
        var collection = _dataStore.GetCollection<Player>("player");
        using var cursors = await collection.FindAsync(new BsonDocument());
        return cursors.ToList().Where(x => x != null).ToArray();
    }
    private async Task<Referee[]> GetReferees()
    {
        var collection = _dataStore.GetCollection<Referee>("referee");
        using var cursors = await collection.FindAsync(new BsonDocument());
        return cursors.ToList().Where(x => x != null).ToArray();
    }
    private async Task<Coach[]> GetCoaches()
    {
        var collection = _dataStore.GetCollection<Coach>("coach");
        using var cursors = await collection.FindAsync(new BsonDocument());
        return cursors.ToList().Where(x => x != null).ToArray();
    }
    public override async Task<AuthenticationState> GetAuthenticationStateAsync()
    {
        var token = await _localStorageService.GetAsync<SecurityToken>(nameof(SecurityToken));
        Config.SecurityToken = token;
        if (token == null)
        {
            return ReturnAnonymous();
        }

        if (string.IsNullOrEmpty(token.AccessToken) || token.ExpiredAt < DateTime.UtcNow)
        {
            return ReturnAnonymous();
        }

        var persons = await GetPersons();
        //var players = await GetPlayers();
        var referees = await GetReferees();
        var coaches = await GetCoaches();

        var person = persons.First(x => x.Login == token.UserName);
        Config.UserName = person.Name;
        var personId = person.Id;

        //ReturnUserState
        //TODO: ������������ �� �����

        var claims = new List<Claim>()
        {
            new Claim(ClaimTypes.Country, "Russia"),
            new Claim(ClaimTypes.Name, token.UserName),
            new Claim(ClaimTypes.Expired, token.ExpiredAt.ToLongDateString()),
            //new Claim(ClaimTypes.Role, "Admin"),
            new Claim(ClaimTypes.Role, "User"),
        };

        if (referees.Any(x => x.PersonId == personId))
        {
            claims.Add(new Claim(ClaimTypes.Role, "Referee"));
        }
        else if (coaches.Any(x => x.PersonId == personId))
        {
            claims.Add(new Claim(ClaimTypes.Role, "Coach"));
        }
        else if (token.UserName == "root")
        {
            claims.Add(new Claim(ClaimTypes.Role, "Admin"));
        }

        var identity = new ClaimsIdentity(claims, "Token");
        var principal = new ClaimsPrincipal(identity);
        return new AuthenticationState(principal);
    }
    private AuthenticationState ReturnAnonymous()
    {
        var anonymousIdentity = new ClaimsIdentity();
        var anonymousPrincipal = new ClaimsPrincipal(anonymousIdentity);
        return new AuthenticationState(anonymousPrincipal);
    }

    public async Task MakeUserAnonymous()
    {
        await _localStorageService.RemoveAsync(nameof(SecurityToken));
        NotifyAuthenticationStateChanged(Task.FromResult(ReturnAnonymous()));
    }
}