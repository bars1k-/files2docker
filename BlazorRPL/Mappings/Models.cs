﻿using MongoDB.Bson.Serialization.Attributes;

namespace BlazorRPL.Mappings
{
    using MongoDB.Bson;

    public class Entity
    {
        public ObjectId Id { get; set; }
    }

    public enum PositionEnum 
    {
        Goalkeeper = 1,
        FieldPlayer
    }
    public class Person : Entity
    {
        public string Name { get; set; }
        public DateTime? Disqualified { get; set; }
        public string Image { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }

    public class Coach : Entity
    {
        [BsonElement("person")]
        public ObjectId PersonId { get; set; }
        public string Country { get; set; }
        public string License { get; set; }
    }
    public class Player : Entity
    {
        [BsonElement("person")]
        public ObjectId PersonId { get; set; }
        public string Country { get; set; }
        public string NameOnShirt { get; set; }
        [BsonRepresentation(BsonType.Int32)]
        public PositionEnum Position { get; set; }
        public int YellowCards { get; set; }
        public int RedCards { get; set; }
        public int? MinutesPlayed { get; set; }
        public int? GoalsScored { get; set; }
        public int? GoalsPenalty { get; set; }
        public int? GoalsMissed { get; set; }
    }
    public class Referee : Entity
    {
        [BsonElement("person")]
        public ObjectId PersonId { get; set; }
        public string City { get; set; }
    }

    public class Club : Entity
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string Image { get; set; }
        [BsonElement("coach")]
        public ObjectId CoachId { get; set; }
        public List<PlayerClubModel> Players { get; set; }
    }

    public class PlayerClubModel
    {
        [BsonElement("player")]
        public ObjectId PlayerId { get; set; }
        public int Number { get; set; }
    }

    public class Match : Entity
    {
        [BsonElement("homeClub")]
        public ObjectId HomeClubId { get; set; }
        [BsonElement("awayClub")]
        public ObjectId AwayClubId { get; set; }
        public SquadModel? SquadHome { get; set; }
        public SquadModel? SquadAway { get; set; }
        [BsonElement("referee")]
        public ObjectId RefereeId { get; set; }
        public int Round { get; set; }
        public DateTime? MatchTime { get; set; }
        public int? HomeScore { get; set; }
        public int? AwayScore { get; set; }
        public ProtocoleModel? Protocole { get; set; }
    }

    public class SquadModel
    {
        public int[] Start { get; set; }
        public int[] Subs { get; set; }
    }

    public class ProtocoleModel
    {
        public int Added1 { get; set; }
        public int Added2 { get; set; }
        public GoalModel[] GoalsHome { get; set; }
        public GoalModel[] GoalsAway { get; set; }
        public CardModel[] YellowCardsHome { get; set; }
        public CardModel[] YellowCardsAway { get; set; }
        public CardModel[] RedCardsHome { get; set; }
        public CardModel[] RedCardsAway { get; set; }
        public SubstitutionModel[] SubstitutionsHome { get; set; }
        public SubstitutionModel[] SubstitutionsAway { get; set; }
        public bool IsHomeCoachDisqualified { get; set; }
        public bool IsAwayCoachDisqualified { get; set; }
    }

    public class GoalModel
    {
        public int Number { get; set; }
        public string Minute { get; set; }
        public bool IsPenalty { get; set; }
    }

    public class CardModel
    {
        public int Number { get; set; }
        public string Minute { get; set; }
    }

    public class SubstitutionModel
    {
        public int NumberOut { get; set; }
        public int NumberIn { get; set; }
        public string Minute { get; set; }
    }

    public class Transfer : Entity
    {
        [BsonElement("person")]
        public ObjectId PersonId { get; set; }
        [BsonElement("from")]
        public ObjectId FromClubId { get; set; }
        [BsonElement("to")]
        public ObjectId ToClubId { get; set; }
        public DateTime Date { get; set; }
    }

    public class DisqualificationVerdict : Entity
    {
        [BsonElement("person")]
        public ObjectId PersonId { get; set; }
        public string Reason { get; set; }
        public DateTime DisqualifiedFrom { get; set; }
        public DateTime DisqualifiedTo { get; set; }
    }

    public class ClubFine : Entity
    {
        [BsonElement("club")]
        public ObjectId ClubId { get; set; }
        public string Reason { get; set; }
        public int FinePts { get; set; }
    }
}
