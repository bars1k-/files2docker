﻿namespace BlazorRPL.Data
{
    public class UserState
    {
        public string? Login { get; set; }
        public string? Password { get; set; }
        public string? Name { get; set; }
        public UserRole Role { get; set; }
        public bool Failed { get; set; }
    }

    public enum UserRole
    {
        Unauthorized = 0,
        Admin, 
        Player,
        Referee,
        Coach
    }
}
