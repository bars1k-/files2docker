﻿using static BlazorRPL.Pages.GenerateCalendar;

namespace BlazorRPL.Data
{
    public class CalendarGenerationService
    {
        public MatchFinale[] GetCalendar(Team[] teams, Team[] top5Teams, Team[] ecTeams)
        {
            var listFinale = new List<MatchFinale>();
            while (true)
            {
                Match?[][] matchesTable =
                {
                    new Match?[15], new Match?[15], new Match?[15], new Match?[15],
                    new Match?[15], new Match?[15], new Match?[15], new Match?[15],
                    new Match?[15], new Match?[15], new Match?[15], new Match?[15],
                    new Match?[15], new Match?[15], new Match?[15], new Match?[15],
                };
                int[] toursBeforeEuroCups = { 2, 7, 9, 11, 13 };
                int[] regularTours = { 1, 3, 4, 5, 6, 8, 10, 12, 14, 15 };
                Match[] matchesBetweenTop5 = new Match[9];

                Match[] possibleCentralMatches = new Match[]
                {
                    new Match(new Team("Зенит", "Санкт-Петербург"), new Team("Краснодар", "Краснодар")),
                    new Match(new Team("Зенит", "Санкт-Петербург"), new Team("ЦСКА", "Москва")),
                    new Match(new Team("Зенит", "Санкт-Петербург"), new Team("Локомотив", "Москва")),
                    new Match(new Team("Зенит", "Санкт-Петербург"), new Team("Спартак", "Москва")),

                    new Match(new Team("Краснодар", "Краснодар"), new Team("ЦСКА", "Москва")),
                    new Match(new Team("Краснодар", "Краснодар"), new Team("Локомотив", "Москва")),
                    new Match(new Team("Краснодар", "Краснодар"), new Team("Спартак", "Москва")),

                    new Match(new Team("ЦСКА", "Москва"), new Team("Локомотив", "Москва")),
                    new Match(new Team("ЦСКА", "Москва"), new Team("Спартак", "Москва")),

                    new Match(new Team("Локомотив", "Москва"), new Team("Спартак", "Москва")),
                };

                //possibleCentralMatches = ShuffleAndPopOne(possibleCentralMatches);
                //В приоритете - еврокубковые туры
                for (var i = 1; i <= teams.Length; i++)
                {
                    for (var tourIndex = 0; tourIndex < toursBeforeEuroCups.Length; tourIndex++)
                    {
                        var team = teams[i - 1];
                        var matches = matchesTable[i - 1];

                        if (matches[toursBeforeEuroCups[tourIndex] - 1] == null)
                        {
                            var random = new Random();
                            var nonFreeTeams = matches
                                .Where(x => x != null)
                                .Select(x => x.T1.Name != team.Name ? x.T1.Name : x.T2.Name)
                                .ToArray(); //Уже взятые команды
                            var teamsWithMatchInTour = GetTeamsInTourWithMatch(toursBeforeEuroCups[tourIndex] - 1);

                            var freeTeams = teams // нужна фильтрация по матчам в туре
                                .Where(x => x.Name != team.Name && !nonFreeTeams.Contains(x.Name) &&
                                            !teamsWithMatchInTour.Contains(x.Name))
                                .ToArray();
                            if (ecTeams.Contains(team))
                            {
                                freeTeams = freeTeams.Except(top5Teams).ToArray();
                            }

                            if (freeTeams.Any())
                            {
                                var indexToTake = random.Next(freeTeams.Length);
                                var takenTeam = freeTeams[indexToTake];
                                matches[toursBeforeEuroCups[tourIndex] - 1] = new Match(team, takenTeam);
                                var matchesOfTakenTeam = matchesTable[Array.IndexOf(teams, takenTeam)];
                                matchesOfTakenTeam[toursBeforeEuroCups[tourIndex] - 1] = new Match(takenTeam, team);
                            }
                        }
                    }
                }

                //Центральные матчи
                for (var tourIndex = 0; tourIndex < regularTours.Length; tourIndex++)
                {
                    var centralMatch = possibleCentralMatches[tourIndex];
                    var indexT1 = Array.IndexOf(teams, centralMatch.T1);
                    var indexT2 = Array.IndexOf(teams, centralMatch.T2);
                    matchesTable[indexT1][regularTours[tourIndex] - 1] = centralMatch;
                    matchesTable[indexT2][regularTours[tourIndex] - 1] = new Match(centralMatch.T2, centralMatch.T1);
                }

                for (var tourIndex = 0; tourIndex < regularTours.Length; tourIndex++)
                {
                    for (var i = 1; i <= teams.Length; i++)
                    {
                        var team = teams[i - 1];
                        var matches = matchesTable[i - 1];
                        if (matches[regularTours[tourIndex] - 1] == null)
                        {
                            var random = new Random();
                            var nonFreeTeams = matches
                                .Where(x => x != null)
                                .Select(x => x.T1.Name != team.Name ? x.T1.Name : x.T2.Name)
                                .ToArray(); //Уже взятые команды
                            var teamsWithMatchInTour = GetTeamsInTourWithMatch(regularTours[tourIndex] - 1);
                            if (teamsWithMatchInTour.Length == 14)
                            {
                                var lastTeam = teams
                                    .Where(x => !teamsWithMatchInTour.Contains(x.Name) && x.Name != team.Name)
                                    .FirstOrDefault();
                                matches[regularTours[tourIndex] - 1] = new Match(team, lastTeam);
                                var matchesOfTakenTeam = matchesTable[Array.IndexOf(teams, lastTeam)];
                                matchesOfTakenTeam[regularTours[tourIndex] - 1] = new Match(lastTeam, team);
                                continue;
                            }

                            var freeTeams = teams // нужна фильтрация по матчам в туре
                                .Where(x => x.Name != team.Name && !nonFreeTeams.Contains(x.Name) &&
                                            !teamsWithMatchInTour.Contains(x.Name))
                                .ToArray();

                            if (freeTeams.Any())
                            {
                                var indexToTake = random.Next(freeTeams.Length);
                                var takenTeam = freeTeams[indexToTake];
                                matches[regularTours[tourIndex] - 1] = new Match(team, takenTeam);
                                var matchesOfTakenTeam = matchesTable[Array.IndexOf(teams, takenTeam)];
                                matchesOfTakenTeam[regularTours[tourIndex] - 1] = new Match(takenTeam, team);
                            }
                        }
                    }
                }

                var is15 = new List<bool>();
                //Вывод
                for (var i = 0; i < matchesTable.Length; i++)
                {
                    var matchesForTeam = matchesTable[i];
                    var teamNames = matchesForTeam.Select(x => x != null ? ConvertToThreeChars(x.T2.Name) : "???").ToArray();

                    is15.Add(teamNames.Count(x => x != "???") == 15 && matchesForTeam.Select(x => ConvertToThreeChars(x.T2.Name)).Distinct().Count() == 15);
                }
                var isGoodAtAllTours = true;
                if (is15.All(x => x))
                {
                    for (var i = 0; i < 15; i++)
                    {
                        if (matchesTable.Select(x => x[i].T1.Name).Distinct().Count() != 16)
                        {
                            isGoodAtAllTours = false;
                        }
                    }

                    if (isGoodAtAllTours)
                    {
                        Match?[][] finishedTable =
                        {
                            new Match?[8], new Match?[8], new Match?[8], new Match?[8], new Match?[8],
                            new Match?[8], new Match?[8], new Match?[8], new Match?[8], new Match?[8],
                            new Match?[8], new Match?[8], new Match?[8], new Match?[8], new Match?[8],
                        };
                        List<DataAbout2PlusByTours> data = new List<DataAbout2PlusByTours>();
                        for (var i = 1; i <= 15; i++)
                        {
                            data.Add(new DataAbout2PlusByTours
                            {
                                Tour = 1,
                                OnlyHome = new List<Team>(),
                                OnlyAway = new List<Team>(),
                                AnyWay = new List<Team>(),
                            });
                        }

                        var isBroken = false;
                        for (var i = 0; i < finishedTable.Length; i++)
                        {
                            var possibleMatchesForTour = matchesTable.Select(x => x[i]).ToArray();

                            var onlyHome = i < 2
                                    ? new Team[] { }
                                    : teams.Where(x =>
                                        finishedTable[i - 1].Any(y => y.T2.Name == x.Name) &&
                                        finishedTable[i - 2].Any(y => y.T2.Name == x.Name)).ToArray();
                            var onlyAway = i < 2
                                ? new Team[] { }
                                : teams.Where(x =>
                                    finishedTable[i - 1].Any(y => y.T1.Name == x.Name) &&
                                    finishedTable[i - 2].Any(y => y.T1.Name == x.Name)).ToArray();
                            var anyWay = i < 2
                                ? teams
                                : teams.Except(onlyHome).Except(onlyAway).ToArray();
                            var conflictHomeMatches =
                                possibleMatchesForTour.Where(x =>
                                    onlyHome.Select(y => y.Name).Any(y => x.T1.Name == y) &&
                                    onlyHome.Select(y => y.Name).Any(y => x.T2.Name == y)).ToArray();

                            var conflictAwayMatches = possibleMatchesForTour.Where(x =>
                                onlyAway.Select(y => y.Name).Any(y => x.T1.Name == y) &&
                                onlyAway.Select(y => y.Name).Any(y => x.T2.Name == y)).ToArray().ToArray();
                            var anyConflictMatches = true;
                            var startTime = DateTime.Now;
                            do
                            {
                                if (DateTime.Now - startTime >= TimeSpan.FromSeconds(1.5))
                                {
                                    isBroken = true;
                                    break;
                                }
                                if (conflictHomeMatches.Length > 0 || conflictAwayMatches.Length > 0)
                                {
                                    Random r = new Random();
                                    var toFixForHome = conflictHomeMatches.Length / 2;
                                    var toFixForAway = conflictAwayMatches.Length / 2;
                                    if (toFixForHome > 0)
                                    {
                                        var matchConflict = conflictHomeMatches.First();
                                        if (i == 2)
                                        {
                                            var teamToSwap = r.Next(0, 2) == 0 ? matchConflict.T1 : matchConflict.T2;
                                            var matchToSwap = finishedTable[i - 1]
                                                .FirstOrDefault(x => x.T2.Name == teamToSwap.Name);
                                            matchToSwap.T2 = matchToSwap.T1;
                                            matchToSwap.T1 = teamToSwap;
                                        }
                                        else
                                        {
                                            var matchesToCheck = finishedTable[i - 1]
                                                .Where(x =>
                                                    x.T2.Name == matchConflict.T1.Name ||
                                                    x.T2.Name == matchConflict.T2.Name)
                                                .ToList();
                                            if (matchesToCheck.All(x => data[i - 1].OnlyAway.Contains(x.T1)))
                                            {
                                                isBroken = true;
                                                break;
                                            }
                                            else
                                            {
                                                var matchToSwap = matchesToCheck.First(x => !data[i - 1].OnlyAway.Contains(x.T1));
                                                var temp = matchToSwap.T1;
                                                matchToSwap.T1 = matchToSwap.T2;
                                                matchToSwap.T2 = temp;
                                            }
                                        }
                                        RefreshData();
                                    }
                                    else
                                    {
                                        if (toFixForAway > 0)
                                        {
                                            var matchConflict = conflictAwayMatches.First();
                                            if (i == 2)
                                            {
                                                var teamToSwap = r.Next(0, 2) == 0 ? matchConflict.T1 : matchConflict.T2;
                                                var matchToSwap = finishedTable[i - 1]
                                                    .FirstOrDefault(x => x.T1.Name == teamToSwap.Name);
                                                matchToSwap.T1 = matchToSwap.T2;
                                                matchToSwap.T2 = teamToSwap;
                                            }
                                            else
                                            {
                                                var matchesToCheck = finishedTable[i - 1]
                                                    .Where(x =>
                                                        x.T1.Name == matchConflict.T1.Name ||
                                                        x.T1.Name == matchConflict.T2.Name)
                                                    .ToList();
                                                if (matchesToCheck.All(x => data[i - 1].OnlyHome.Contains(x.T2)))
                                                {
                                                    isBroken = true;
                                                    break;
                                                }
                                                else
                                                {
                                                    var matchToSwap = matchesToCheck.First(x => !data[i - 1].OnlyHome.Contains(x.T2));
                                                    var temp = matchToSwap.T1;
                                                    matchToSwap.T1 = matchToSwap.T2;
                                                    matchToSwap.T2 = temp;
                                                }
                                            }
                                            RefreshData();
                                        }
                                    }

                                    void RefreshData()
                                    {
                                        onlyHome = i < 2
                                            ? new Team[] { }
                                            : teams.Where(x =>
                                                finishedTable[i - 1].Any(y => y.T2.Name == x.Name) &&
                                                finishedTable[i - 2].Any(y => y.T2.Name == x.Name)).ToArray();
                                        onlyAway = i < 2
                                            ? new Team[] { }
                                            : teams.Where(x =>
                                                finishedTable[i - 1].Any(y => y.T1.Name == x.Name) &&
                                                finishedTable[i - 2].Any(y => y.T1.Name == x.Name)).ToArray();
                                        anyWay = i < 2
                                            ? teams
                                            : teams.Except(onlyHome).Except(onlyAway).ToArray();

                                        conflictHomeMatches =
                                            possibleMatchesForTour.Where(x =>
                                                onlyHome.Select(y => y.Name).Any(y => x.T1.Name == y) &&
                                                onlyHome.Select(y => y.Name).Any(y => x.T2.Name == y)).ToArray();

                                        conflictAwayMatches = possibleMatchesForTour.Where(x =>
                                            onlyAway.Select(y => y.Name).Any(y => x.T1.Name == y) &&
                                            onlyAway.Select(y => y.Name).Any(y => x.T2.Name == y)).ToArray().ToArray();
                                        anyConflictMatches = conflictAwayMatches.Any() && conflictHomeMatches.Any();
                                    }
                                }
                                else
                                {
                                    anyConflictMatches = false;
                                }
                            } while (anyConflictMatches);

                            if (isBroken)
                            {
                                break;
                            }
                            //Корректировка данных о прошлом туре
                            if (i >= 2)
                            {
                                data[i - 1].OnlyHome = onlyHome.ToList().GetRange(0, onlyHome.Length);
                                data[i - 1].OnlyHome = onlyHome.ToList().GetRange(0, onlyHome.Length);
                                data[i - 1].AnyWay = onlyHome.ToList().GetRange(0, onlyHome.Length);
                            }

                            for (var j = 0; j < finishedTable[i].Length; j++)
                            {
                                var existingMatches = finishedTable[i].Where(x => x != null).ToArray();
                                var teamsAreWaiting = teams
                                    .Where(x => !existingMatches.Any(y => y.T1.Name == x.Name || y.T2.Name == x.Name)).ToArray();

                                Team team1 = null, team2 = null;
                                if (i < 2)
                                {
                                    team1 = Shuffle(teamsAreWaiting).First();
                                    team2 = possibleMatchesForTour.First(x => x.T1.Name == team1.Name).T2;
                                }
                                else
                                {
                                    var trimmedOnlyHome = onlyHome.Intersect(teamsAreWaiting).ToArray();
                                    var trimmedOnlyAway = onlyAway.Intersect(teamsAreWaiting).ToArray();
                                    var trimmedAnyWay = anyWay.Intersect(teamsAreWaiting).ToArray();
                                    if (trimmedOnlyHome.Any())
                                    {
                                        team1 = Shuffle(trimmedOnlyHome).First();
                                        team2 = possibleMatchesForTour.First(x => x.T1.Name == team1.Name).T2;
                                    }
                                    else if (trimmedOnlyAway.Any())
                                    {
                                        team2 = Shuffle(trimmedOnlyAway).First();
                                        team1 = possibleMatchesForTour.First(x => x.T1.Name == team2.Name).T2;
                                    }
                                    else
                                    {
                                        team1 = Shuffle(trimmedAnyWay).First();
                                        team2 = possibleMatchesForTour.First(x => x.T1.Name == team1.Name).T2;
                                    }
                                }

                                var is4MatchesInMoscow = existingMatches.Count(x => x.T1.City == "Москва") == 3;
                                var is4MatchesAwayMoscow = existingMatches.Count(x => x.T2.City == "Москва") == 3;
                                var isT1FromMoscow = team1.City == "Москва" && team2.City != "Москва";
                                var isT2FromMoscow = team1.City != "Москва" && team2.City == "Москва";
                                var isOneMoscowTeam = isT1FromMoscow || isT2FromMoscow;
                                if (isOneMoscowTeam)
                                {
                                    if (is4MatchesInMoscow && isT1FromMoscow)
                                    {
                                        finishedTable[i][j] = possibleMatchesForTour.First(x => x.T2.Name == team1.Name);
                                        continue;
                                    }

                                    if (is4MatchesAwayMoscow && isT2FromMoscow)
                                    {
                                        finishedTable[i][j] = possibleMatchesForTour.First(x => x.T1.Name == team2.Name);
                                        continue;
                                    }
                                    finishedTable[i][j] = possibleMatchesForTour.First(x => x.T1.Name == team1.Name);
                                }
                                else
                                {
                                    finishedTable[i][j] = possibleMatchesForTour.First(x => x.T1.Name == team1.Name);
                                }
                            }

                            if (i < 1)
                            {
                                data[i].AnyWay = teams.ToList().GetRange(0, teams.Length);
                            }
                            else
                            {
                                data[i].OnlyHome = teams.Where(x =>
                                    finishedTable[i].Any(y => y.T2.Name == x.Name) &&
                                    finishedTable[i - 1].Any(y => y.T2.Name == x.Name)).ToList();
                                data[i].OnlyAway = teams.Where(x =>
                                    finishedTable[i].Any(y => y.T1.Name == x.Name) &&
                                    finishedTable[i - 1].Any(y => y.T1.Name == x.Name)).ToList();
                                data[i].AnyWay = teams.Except(onlyHome).Except(onlyAway).ToList();
                            }
                        }

                        if (isBroken)
                        {
                            continue;
                        }

                        for (var i = 0; i < finishedTable.Length; i++)
                        {
                            foreach (var match in finishedTable[i])
                            {
                                listFinale.Add(new MatchFinale()
                                {
                                    Team1 = match.T1.Name,
                                    Team2 = match.T2.Name,
                                    Tour = i + 1
                                });
                            }
                        }

                        break;
                    }

                }

                string[] GetTeamsInTourWithMatch(int tour) => matchesTable.Select(x => x[tour]?.T1.Name ?? "").Where(x => x != "").ToArray();
            }

            return listFinale.ToArray();
        }
        string ConvertToThreeChars(string teamName) => teamName switch
        {
            "Зенит" => "ЗЕН",
            "Сочи" => "СОЧ",
            "Динамо" => "ДИН",
            "ЦСКА" => "ЦСК",
            "Локомотив" => "ЛОК",
            "Спартак" => "СПА",
            "Торпедо" => "ТОР",
            "Краснодар" => "КРА",
            "Ахмат" => "АХМ",
            "Крылья Советов" => "КС ",
            "Ростов" => "РОС",
            "Пари НН" => "НН ",
            "Урал" => "УРА",
            "Химки" => "ХИМ",
            "Факел" => "ФАК",
            "Оренбург" => "ОРЕ",
            _ => "???"
        };

        static T[] Shuffle<T>(T[] arr)
        {
            // создаем экземпляр класса Random для генерирования случайных чисел
            Random rand = new Random();

            for (int i = arr.Length - 1; i >= 1; i--)
            {
                int j = rand.Next(i + 1);

                T tmp = arr[j];
                arr[j] = arr[i];
                arr[i] = tmp;
            }

            return arr;
        }

        static T[] ShuffleAndPopOne<T>(T[] arr)
        {
            var result = Shuffle(arr).ToList();
            result.RemoveAt(0);
            return result.ToArray();
        }

        public class Team
        {
            public Team(string name, string city)
            {
                Name = name;
                City = city;
            }
            public string Name { get; set; }
            public string City { get; set; }

            public override int GetHashCode()
            {
                return this.Name.GetHashCode();
            }
            public override bool Equals(object? obj)
            {
                if (obj == null) return false;
                if (obj is Team t2)
                {
                    return this.Name == t2.Name && this.City == t2.City;
                }
                return false;
            }
        }

        public class Match
        {
            public Match(Team t1, Team t2)
            {
                T1 = t1;
                T2 = t2;
            }

            public Team T1 { get; set; }
            public Team T2 { get; set; }
        }

        public class DataAbout2PlusByTours
        {
            public int Tour { get; set; }
            public List<Team> OnlyHome { get; set; }
            public List<Team> OnlyAway { get; set; }
            public List<Team> AnyWay { get; set; }
        }
    }
}
