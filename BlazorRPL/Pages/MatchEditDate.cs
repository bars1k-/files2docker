﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using BlazorRPL.Mappings;
using Microsoft.AspNetCore.Components;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorRPL.Pages
{
    public class MatchEditDate : ComponentBase
    {
        public MongoClient Db { get; set; }
        private IMongoDatabase dataStore;
        private IMongoCollection<Match> matchCollection;
        public MatchEditDateViewModel MatchEditDateViewModel { get; set; }
        [Inject] public NavigationManager NavigationManager { get; set; }
        private Person[] persons { get; set; }
        private DisqualificationVerdict[] disqualifications { get; set; }

        public MatchEditDate()
        {
            MatchEditDateViewModel = new MatchEditDateViewModel();
            Db = new MongoClient(Config.ConnectionString);
            dataStore = Db.GetDatabase("premierleague");
            matchCollection = dataStore.GetCollection<Match>("match");
            persons = GetPersons();
            disqualifications = GetDisqualificationVerdicts();
        }

        public async Task GetMatchesInTourAsync()
        {
            using var cursors = await matchCollection.FindAsync(new BsonDocument());
            var matches = cursors.ToList().Where(x => x != null).ToArray();
            MatchEditDateViewModel.Clubs = await GetClubs();
            MatchEditDateViewModel.MatchesForTour = matches.Where(x => x.Round == MatchEditDateViewModel.Tour).Select(x => new MatchModel
            {
                Match = x,
                RefereeIdString = x.RefereeId.ToString() ?? "",
            }).ToArray();
            var length = MatchEditDateViewModel.MatchesForTour.Length;
            for (var i = 0; i < length; i++)
            {
                if (MatchEditDateViewModel.MatchesForTour[i].Match.MatchTime == null)
                {
                    MatchEditDateViewModel.MatchesForTour[i].Match.MatchTime = DateTime.MinValue;
                }
            }

            MatchEditDateViewModel.IsSuccess = false;
            MatchEditDateViewModel.AvailableReferees = await GetReferees();
        }

        public async Task EditMatchesAsync()
        {
            var matches = MatchEditDateViewModel.MatchesForTour.Select(x => x).ToArray();
            //Проверить загруженность арбитра на матче, и отсутствие у него дисквалификации
            if (matches.All(x => x.Match.Round == MatchEditDateViewModel.Tour))
            {
                var refereeIdsInMatch = matches.Select(x => x.RefereeIdString).ToArray();
                var referees = MatchEditDateViewModel.AvailableReferees.Where(x => refereeIdsInMatch.Contains(x.Id.ToString())).ToArray();
                if (matches.Length == referees.Distinct().Count())
                {
                    foreach (var match in matches.Where(x => x.Match.MatchTime != DateTime.MinValue))
                    {
                        var flag = true;
                        var referee = referees.FirstOrDefault(x => x.Id == ObjectId.Parse(match.RefereeIdString));
                        if (referee != null)
                        {
                            var hasDisqualification = disqualifications.Any(x => x.PersonId == referee.Person.Id &&
                                x.DisqualifiedTo > match.Match.MatchTime);
                            flag = hasDisqualification;
                        }
                        var filter = Builders<Match>.Filter.Eq("_id", match.Match.Id);
                        var update = flag
                            ? Builders<Match>.Update.Set("matchTime", match.Match.MatchTime.Value.AddHours(3))
                            : Builders<Match>.Update
                                .Set("matchTime", match.Match.MatchTime.Value.AddHours(3))
                                .Set("referee", ObjectId.Parse(match.RefereeIdString));
                        await matchCollection.UpdateOneAsync(filter, update);
                        MatchEditDateViewModel.IsSuccess = true;
                    }
                }
            }

            await GetMatchesInTourAsync();
        }

        private async Task<Club[]> GetClubs()
        {
            var collection = dataStore.GetCollection<Club>("club");
            using var cursors = await collection.FindAsync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }

        private async Task<RefereePerson[]> GetReferees()
        {
            var collection = dataStore.GetCollection<Referee>("referee");
            using var cursors = await collection.FindAsync(new BsonDocument());
            var referees = cursors.ToList().Where(x => x != null).ToArray();
            return referees.Select(x => new RefereePerson
            {
                Person = persons.First(y => y.Id == x.PersonId),
                City = x.City,
                Id = x.Id,
            }).ToArray();
        }
        private Person[] GetPersons()
        {
            var collection = dataStore.GetCollection<Person>("person");
            using var cursors = collection.FindSync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }

        private DisqualificationVerdict[] GetDisqualificationVerdicts()
        {
            var disqCollection = dataStore.GetCollection<DisqualificationVerdict>("disqualificationVerdict");
            using var disqCursors = disqCollection.FindSync(new BsonDocument());
            return disqCursors.ToList().ToArray();
        }
    }

    public class MatchModel
    {
        public Match Match { get; set; }
        public string RefereeIdString { get; set; }
    }

    public class MatchEditDateViewModel
    {
        [Required]
        public int Tour { get; set; }
        public MatchModel[] MatchesForTour { get; set; }
        public Club[] Clubs { get; set; }
        public bool IsSuccess { get; set; }
        public RefereePerson[] AvailableReferees { get; set; }
    }

    public class RefereePerson
    {
        public Person Person { get; set; }
        public string City { get; set; }
        public ObjectId Id { get; set; }
    }
}
