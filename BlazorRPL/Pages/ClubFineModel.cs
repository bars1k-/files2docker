﻿using System.ComponentModel.DataAnnotations;
using BlazorRPL.Mappings;
using Microsoft.AspNetCore.Components;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorRPL.Pages
{
    public class ClubFineModel : ComponentBase
    {
        public MongoClient Db { get; set; }
        private IMongoDatabase dataStore;
        public ClubFineViewModel ClubFineViewModel { get; set; }
        [Inject] public NavigationManager NavigationManager { get; set; }

        public ClubFineModel()
        {
            ClubFineViewModel = new ClubFineViewModel();
            Db = new MongoClient(Config.ConnectionString);
            dataStore = Db.GetDatabase("premierleague");
        }
        public async Task RegisterFineAsync()
        {
            if (string.IsNullOrEmpty(ClubFineViewModel.Team) || string.IsNullOrEmpty(ClubFineViewModel.Reason) || ClubFineViewModel.Points <= 0)
            {
                NavigationManager.NavigateTo("/PointsFine");
                return;
            }
            var collection = dataStore.GetCollection<Club>("club");
            var collectionFine = dataStore.GetCollection<ClubFine>("teamFine");
            using var cursors = await collection.FindAsync(new BsonDocument());
            var clubs = cursors.ToList().Where(x => x != null).ToArray();
            var club = clubs.FirstOrDefault(x => x.Name == ClubFineViewModel.Team);
            var clubFine = new ClubFine
            {
                ClubId = club.Id,
                Reason = ClubFineViewModel.Reason,
                FinePts = ClubFineViewModel.Points
            };
            await collectionFine.InsertOneAsync(clubFine);
            NavigationManager.NavigateTo("/FineSuccessAdded");
        }
    }

    public class ClubFineViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите наименование команды!")]
        public string Team { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите причину!")]
        public string Reason { get; set; }
        [Required]
        public int Points { get; set; }
    }
}
