﻿using BlazorRPL.Mappings;
using Microsoft.AspNetCore.Components;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorRPL.Pages
{
    public class PlayersByClubModel : ComponentBase
    {
        public MongoClient Db { get; set; }
        private IMongoDatabase dataStore;
        public PlayersByClubViewModel PlayersByClubViewModel { get; set; }

        public PlayersByClubModel()
        {
            Db = new MongoClient(Config.ConnectionString);
            dataStore = Db.GetDatabase("premierleague");
            PlayersByClubViewModel = new PlayersByClubViewModel();
            PlayersByClubViewModel.Players = new PlayerWithNumber[] { };
            PlayersByClubViewModel.Clubs = GetClubs();
            foreach (var club in PlayersByClubViewModel.Clubs)
            {
                Console.WriteLine(club.Name);
            }
        }
        private Club[] GetClubs()
        {
            var collection = dataStore.GetCollection<Club>("club");
            using var cursors = collection.FindSync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        public async Task GetPlayersForClub()
        {
            PlayersByClubViewModel.Matches = (await GetMatches()).Where(x =>
                    x.AwayClubId == ObjectId.Parse(PlayersByClubViewModel.ClubId) || 
                    x.HomeClubId == ObjectId.Parse(PlayersByClubViewModel.ClubId))
                .ToArray();
            var allPlayers = await GetPlayers();
            var playersInClub = (await GetClub()).Players;
            PlayersByClubViewModel.Players = playersInClub.Select(x => new PlayerWithNumber
            {
                Number = x.Number,
                Player = allPlayers.First(y => y.Id == x.PlayerId)
            }).OrderBy(x => x.Player.Position).ThenBy(x => x.Number).ToArray();
        }

        private async Task<Club> GetClub()
        {
            var collection = dataStore.GetCollection<Club>("club");
            using var cursors = await collection.FindAsync(new BsonDocument{ {"_id", ObjectId.Parse(PlayersByClubViewModel.ClubId)} });
            return cursors.ToList().First();
        }
        private async Task<Player[]> GetPlayers()
        {
            var collection = dataStore.GetCollection<Player>("player");
            using var cursors = await collection.FindAsync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private async Task<Match[]> GetMatches()
        {
            var collection = dataStore.GetCollection<Match>("match");
            using var cursors = await collection.FindAsync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
    }

    public class PlayersByClubViewModel
    {
        public PlayerWithNumber[] Players { get; set; }
        public string ClubId { get; set; }
        public Match[] Matches { get; set; }
        public Club[] Clubs { get; set; }
    }
}
