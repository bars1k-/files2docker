﻿using BlazorRPL.Mappings;
using Microsoft.AspNetCore.Components;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorRPL.Pages
{
    public class ShowMatchesModel : ComponentBase
    {
        public ShowMatchesViewModel ShowMatchesViewModel { get; set; }
        public MongoClient Db { get; set; }
        private IMongoDatabase dataStore;
        private Match[] AllMatches { get; set; }
        public ShowMatchesModel()
        {
            ShowMatchesViewModel = new ShowMatchesViewModel();
            ShowMatchesViewModel.Matches = new Match[] {};
            Db = new MongoClient(Config.ConnectionString);
            dataStore = Db.GetDatabase("premierleague");
            AllMatches = GetMatches();
            ShowMatchesViewModel.Clubs = GetClubs();
            ShowMatchesViewModel.Players = GetPlayers();
        }

        public void ShowMatchesByRound()
        {
            ShowMatchesViewModel.Matches = AllMatches.Where(x => x.Round == ShowMatchesViewModel.Round).ToArray();
        }

        private Match[] GetMatches()
        {
            var collection = dataStore.GetCollection<Match>("match");
            using var cursors = collection.FindSync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private Club[] GetClubs()
        {
            var collection = dataStore.GetCollection<Club>("club");
            using var cursors = collection.FindSync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private Player[] GetPlayers()
        {
            var collection = dataStore.GetCollection<Player>("player");
            using var cursors = collection.FindSync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
    }

    public class ShowMatchesViewModel
    {
        public Match[] Matches { get; set; }
        public int Round { get; set; }
        public Club[] Clubs { get; set; }
        public Player[] Players { get; set; }
    }
}
