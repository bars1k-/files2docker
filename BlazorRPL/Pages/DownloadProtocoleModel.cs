﻿using System.Runtime.CompilerServices;
using System.Text;
using BlazorRPL.Mappings;
using GemBox.Spreadsheet;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorRPL.Pages
{
    public class DownloadProtocoleModel : ComponentBase
    {
        public MongoClient Db { get; set; }
        private IMongoDatabase dataStore;
        public DownloadProtocoleViewModel DownloadProtocoleViewModel { get; set; }
        [Inject] public NavigationManager NavigationManager { get; set; }

        [Inject] public IJSRuntime Js { get; set; }

        public DownloadProtocoleModel()
        {
            DownloadProtocoleViewModel = new DownloadProtocoleViewModel();
            Db = new MongoClient(Config.ConnectionString);
            dataStore = Db.GetDatabase("premierleague");
            var currentPersonRef = GetPersons().First(x => x.Login == Config.SecurityToken.UserName);
            var referee = GetReferees().First(x => x.PersonId == currentPersonRef.Id);
            DownloadProtocoleViewModel.PossibleMatches = GetMatches()
                .Where(x => x.RefereeId == referee.Id && x.SquadHome != null && x.SquadAway != null && x.Protocole == null &&
                            x.SquadHome.Start.Any() && x.SquadAway.Start.Any()).ToArray();
            var clubDict = new Dictionary<ObjectId, string>();
            var clubs = GetClubsSync();
            foreach (var club in clubs)
            {
                clubDict[club.Id] = club.Name;
            }

            DownloadProtocoleViewModel.TeamDictionary = clubDict;
        }

        public async Task DownloadExcel()
        {
            ExcelFile file = ExcelFile.Load($"{Environment.CurrentDirectory}/wwwroot/PROTOCOLE_EXAMPLE.xlsx");
            var cells = file.Worksheets[0].Cells;
            var match = DownloadProtocoleViewModel.PossibleMatches.First(x =>
                x.Id == ObjectId.Parse(DownloadProtocoleViewModel.ObjectIdStringSelected));
            var persons = await GetPersonsAsync();
            var clubs = await GetClubs();
            var players = await GetPlayers();
            var coaches = await GetCoaches();

            var homeClub = clubs.First(x => x.Id == match.HomeClubId);
            var awayClub = clubs.First(x => x.Id == match.AwayClubId);

            var homeStart = match.SquadHome.Start.Select(x => new PlayerWithNumber
            {
                Player = players.First(z => z.Id == homeClub.Players.First(y => y.Number == x).PlayerId),
                Number = x
            }).ToArray();
            var homeSubs = match.SquadHome.Subs.Select(x => new PlayerWithNumber
            {
                Player = players.First(z => z.Id == homeClub.Players.First(y => y.Number == x).PlayerId),
                Number = x
            }).ToArray();
            var awayStart = match.SquadAway.Start.Select(x => new PlayerWithNumber
            {
                Player = players.First(z => z.Id == awayClub.Players.First(y => y.Number == x).PlayerId),
                Number = x
            }).ToArray();
            var awaySubs = match.SquadAway.Subs.Select(x => new PlayerWithNumber
            {
                Player = players.First(z => z.Id == awayClub.Players.First(y => y.Number == x).PlayerId),
                Number = x
            }).ToArray();

            cells["A1"].Value = match.MatchTime ?? DateTime.Now.Date;
            cells["D1"].Value = match.Round;
            cells["D2"].Value = DownloadProtocoleViewModel.TeamDictionary[match.HomeClubId];
            cells["D3"].Value = DownloadProtocoleViewModel.TeamDictionary[match.AwayClubId];

            for (var i = 8; i <= 26; i++)
            {
                if (i < 19)
                {
                    cells[$"A{i}"].Value = homeStart[i - 8].Player.NameOnShirt;
                    cells[$"B{i}"].Value = homeStart[i - 8].Number;

                    cells[$"A{i + 21}"].Value = awayStart[i - 8].Player.NameOnShirt;
                    cells[$"B{i + 21}"].Value = awayStart[i - 8].Number;
                }
                else if (i > 19)
                {
                    cells[$"A{i}"].Value = homeSubs[i - 20].Player.NameOnShirt;
                    cells[$"B{i}"].Value = homeSubs[i - 20].Number;

                    cells[$"A{i + 21}"].Value = awaySubs[i - 20].Player.NameOnShirt;
                    cells[$"B{i + 21}"].Value = awaySubs[i - 20].Number;
                }
            }

            cells["A50"].Value = persons.First(y => y.Id == coaches.First(x => x.Id == homeClub.CoachId).PersonId).Name;
            cells["A52"].Value = persons.First(y => y.Id == coaches.First(x => x.Id == awayClub.CoachId).PersonId).Name;

            file.Save($"{Environment.CurrentDirectory}/wwwroot/{DownloadProtocoleViewModel.ObjectIdStringSelected}.xlsx");
            var fs = File.OpenRead(
                $"{Environment.CurrentDirectory}/wwwroot/{DownloadProtocoleViewModel.ObjectIdStringSelected}.xlsx");
            using var streamRef = new DotNetStreamReference(stream: fs);
            await Js.InvokeVoidAsync("downloadFileFromStream", $"{DownloadProtocoleViewModel.ObjectIdStringSelected}.xlsx", streamRef);
            File.Delete($"{Environment.CurrentDirectory}/wwwroot/{DownloadProtocoleViewModel.ObjectIdStringSelected}.xlsx");
        }

        private Person[] GetPersons()
        {
            var collection = dataStore.GetCollection<Person>("person");
            using var cursors = collection.FindSync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private async Task<Person[]> GetPersonsAsync()
        {
            var collection = dataStore.GetCollection<Person>("person");
            using var cursors = await collection.FindAsync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private Referee[] GetReferees()
        {
            var collection = dataStore.GetCollection<Referee>("referee");
            using var cursors = collection.FindSync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private Match[] GetMatches()
        {
            var collection = dataStore.GetCollection<Match>("match");
            using var cursors = collection.FindSync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private async Task<Club[]> GetClubs()
        {
            var collection = dataStore.GetCollection<Club>("club");
            using var cursors = await collection.FindAsync(new BsonDocument());
            var clubs = cursors.ToList().Where(x => x != null).ToArray();
            return clubs;
        }
        private Club[] GetClubsSync()
        {
            var collection = dataStore.GetCollection<Club>("club");
            using var cursors = collection.FindSync(new BsonDocument());
            var clubs = cursors.ToList().Where(x => x != null).ToArray();
            return clubs;
        }
        private async Task<Player[]> GetPlayers()
        {
            var collection = dataStore.GetCollection<Player>("player");
            using var cursors = await collection.FindAsync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private async Task<Coach[]> GetCoaches()
        {
            var collection = dataStore.GetCollection<Coach>("coach");
            using var cursors = await collection.FindAsync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
    }

    public class DownloadProtocoleViewModel 
    {
        public Match[] PossibleMatches { get; set; }
        public string ObjectIdStringSelected { get; set; }
        public Dictionary<ObjectId, string> TeamDictionary { get; set; }
    }
}
