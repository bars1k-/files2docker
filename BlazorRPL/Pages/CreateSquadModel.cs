﻿using BlazorRPL.Mappings;
using Microsoft.AspNetCore.Components;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorRPL.Pages
{
    public class CreateSquadModel : ComponentBase
    {
        public MongoClient Db { get; set; }
        private IMongoDatabase dataStore;
        public CreateSquadViewModel CreateSquadViewModel { get; set; }
        [Inject] public NavigationManager NavigationManager { get; set; }
        private IMongoCollection<Match> matchCollection { get; set; }
        private ObjectId TeamId { get; set; }
        public CreateSquadModel()
        {
            CreateSquadViewModel = new CreateSquadViewModel();
            CreateSquadViewModel.TeamDictionary = new Dictionary<ObjectId, string>()
            {
                [ObjectId.Parse("63a0b011e828aa5288c7a4d0")] = "Зенит",
                [ObjectId.Parse("63a0d160e828aa5288c7a4ef")] = "Ахмат",
                [ObjectId.Parse("63a0d160e828aa5288c7a4f0")] = "Динамо",
                [ObjectId.Parse("63a0d160e828aa5288c7a4f1")] = "Краснодар",
                [ObjectId.Parse("63a0d160e828aa5288c7a4f2")] = "Крылья Советов",
                [ObjectId.Parse("63a0d160e828aa5288c7a4f3")] = "Локомотив",
                [ObjectId.Parse("63a0d160e828aa5288c7a4f4")] = "Оренбург",
                [ObjectId.Parse("63a0d160e828aa5288c7a4f5")] = "Пари НН",
                [ObjectId.Parse("63a0d160e828aa5288c7a4f6")] = "Ростов",
                [ObjectId.Parse("63a0d160e828aa5288c7a4f7")] = "Сочи",
                [ObjectId.Parse("63a0d160e828aa5288c7a4f8")] = "Спартак",
                [ObjectId.Parse("63a0d160e828aa5288c7a4f9")] = "Торпедо",
                [ObjectId.Parse("63a0d160e828aa5288c7a4fa")] = "Урал",
                [ObjectId.Parse("63a0d160e828aa5288c7a4fb")] = "Факел",
                [ObjectId.Parse("63a0d160e828aa5288c7a4fc")] = "Химки",
                [ObjectId.Parse("63a0d160e828aa5288c7a4fd")] = "ЦСКА",
            };
            CreateSquadViewModel.PlayersInSquad = new PlayerWithNumber[18];
            for (var i = 0; i < CreateSquadViewModel.PlayersInSquad.Length; i++)
            {
                CreateSquadViewModel.PlayersInSquad[i] = new PlayerWithNumber
                {
                    Player = new Player(),
                    Number = default
                };
            }
            Db = new MongoClient(Config.ConnectionString);
            dataStore = Db.GetDatabase("premierleague");
            var personCollection = dataStore.GetCollection<Person>("person");
            using var personCoachCursorsAsyncCursor = personCollection.FindSync(new BsonDocument { {"login", Config.SecurityToken.UserName} });
            var personCoach = personCoachCursorsAsyncCursor.ToList().First();
            //Нашли тренера по текущему юзеру
            var coachCollection = dataStore.GetCollection<Coach>("coach");
            using var coachCursorsAsyncCursor = coachCollection.FindSync(new BsonDocument { { "person", personCoach.Id } });
            var coach = coachCursorsAsyncCursor.ToList().First();
            //Ищем клуб по тренеру
            var clubCollection = dataStore.GetCollection<Club>("club");
            using var clubAsyncCursor = clubCollection.FindSync(new BsonDocument { { "coach", coach.Id } });
            var club = clubAsyncCursor.ToList().First();
            TeamId = club.Id;
            matchCollection = dataStore.GetCollection<Match>("match");
            using var macthCursors = matchCollection.FindSync(new BsonDocument());
            var matches = macthCursors.ToList().Where(x => (x.HomeClubId == club.Id || x.AwayClubId == club.Id) && 
                                                           x.MatchTime.HasValue && x.MatchTime.Value > DateTime.Now).ToList();
            if (matches.Count > 0)
            {
                CreateSquadViewModel.NextMatch = matches.OrderBy(x => x.MatchTime).First();
                var teamSquadIds = club.Players.Select(x => x.PlayerId).ToArray();
                var playersCollection = dataStore.GetCollection<Player>("player");
                using var playersCursors = playersCollection.FindSync(new BsonDocument());
                var playersInClub = playersCursors.ToList().Where(x => teamSquadIds.Contains(x.Id)).ToList();

                var disqCollection = dataStore.GetCollection<DisqualificationVerdict>("disqualificationVerdict");
                using var disqCursors = disqCollection.FindSync(new BsonDocument { {"disqualifiedTo", new BsonDocument ("$gt", CreateSquadViewModel.NextMatch.MatchTime) }});
                var currentDisaqualifications = disqCursors.ToList();
                var disqualifiedPersons = currentDisaqualifications.Select(x => x.PersonId).ToList();

                //playersInClub = playersInClub.Where(x => !disqualifiedPersons.Contains(x.PersonId)).ToList();
                
                CreateSquadViewModel.PlayersInClub = playersInClub.Select(x => new PlayerWithNumber()
                {
                    Player = x,
                    Number = club.Players.First(y => y.PlayerId == x.Id).Number,
                    IsDisqualified = disqualifiedPersons.Contains(x.PersonId),
                    DisqualifiedTo = disqualifiedPersons.Contains(x.PersonId) ? currentDisaqualifications.First(y => y.PersonId == x.PersonId).DisqualifiedTo : new DateTime(),
                    Reason = disqualifiedPersons.Contains(x.PersonId) ? currentDisaqualifications.First(y => y.PersonId == x.PersonId).Reason : ""
                }).ToArray();
            }
        }

        public async Task CreateSquadAsync()
        {
            //TODO: Проверить, что заставляет выпадать список в багу!!!
            var squadInCore = CreateSquadViewModel.PlayersInSquad.ToList().GetRange(0, 11).ToList();
            var numbersCore = squadInCore.Select(x => x.Number).ToArray();
            var allNumbers = CreateSquadViewModel.PlayersInSquad.Select(x => x.Number);

            if (CreateSquadViewModel.PlayersInSquad.Select(x => x.Number).Distinct().Count() == CreateSquadViewModel.PlayersInSquad.Length && 
                CreateSquadViewModel.PlayersInClub.Where(x => numbersCore.Contains(x.Number)).Count(x => x.Player.Country != "Россия") <= 8 &&
                CreateSquadViewModel.PlayersInClub.Where(x => allNumbers.Contains(x.Number)).Count(x => x.Player.Country != "Россия") <= 13)
            {
                if (CreateSquadViewModel.NextMatch.HomeClubId == TeamId)
                {
                    CreateSquadViewModel.NextMatch.SquadHome = new SquadModel();
                    CreateSquadViewModel.NextMatch.SquadHome.Start = CreateSquadViewModel.PlayersInSquad.ToList()
                        .GetRange(0, 11).Select(x => x.Number).ToArray();
                    CreateSquadViewModel.NextMatch.SquadHome.Subs = CreateSquadViewModel.PlayersInSquad.ToList()
                        .GetRange(11, 7).Select(x => x.Number).ToArray();
                }
                else
                {
                    CreateSquadViewModel.NextMatch.SquadAway = new SquadModel();
                    CreateSquadViewModel.NextMatch.SquadAway.Start = CreateSquadViewModel.PlayersInSquad.ToList()
                        .GetRange(0, 11).Select(x => x.Number).ToArray();
                    CreateSquadViewModel.NextMatch.SquadAway.Subs = CreateSquadViewModel.PlayersInSquad.ToList()
                        .GetRange(11, 7).Select(x => x.Number).ToArray();
                }
                matchCollection.UpdateOneAsync(Builders<Match>.Filter.Eq("_id", CreateSquadViewModel.NextMatch.Id), 
                    Builders<Match>.Update
                    .Set("squadHome", CreateSquadViewModel.NextMatch.SquadHome)
                    .Set("squadAway", CreateSquadViewModel.NextMatch.SquadAway));
                NavigationManager.NavigateTo("/SquadCreated");
            }
        }
    }

    public class CreateSquadViewModel
    {
        public PlayerWithNumber?[] PlayersInSquad { get; set; }
        public PlayerWithNumber[] PlayersInClub { get; set; }
        public Match NextMatch { get; set; }
        public Dictionary<ObjectId, string> TeamDictionary { get; set; }
    }

    public class DisqualificationVerdictModel
    {
        public int Number { get; set; }
        public DateTime DisqualifiedFrom { get; set; }
        public DateTime DisqualifiedTo { get; set; }
    }

    public class PlayerWithNumber
    {
        public Player Player { get; set; }
        public int Number { get; set; }
        public bool IsDisqualified { get; set; }
        public DateTime DisqualifiedTo { get; set; }
        public string Reason { get; set; }
    }
}
