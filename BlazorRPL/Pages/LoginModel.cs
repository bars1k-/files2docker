﻿using System;
using System.ComponentModel.DataAnnotations;
using BlazorRPL.Infrastructure;
using BlazorRPL.Mappings;
using Microsoft.AspNetCore.Components;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorRPL.Pages
{
    public class LoginModel : ComponentBase
    {
        [Inject] public ILocalStorageService LocalStorageService { get; set; }

        [Inject] public NavigationManager NavigationManager { get; set; }
        public MongoClient Db { get; set; }
        private IMongoDatabase dataStore;
        private List<Person> persons = new List<Person>();
        private List<Player> players = new List<Player>();
        private List<Coach> coaches = new List<Coach>();
        private List<Referee> referees = new List<Referee>();

        private async Task<Person[]> GetPersons()
        {
            var collection = dataStore.GetCollection<Person>("person");
            using var cursors = await collection.FindAsync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private async Task<Player[]> GetPlayers()
        {
            var collection = dataStore.GetCollection<Player>("player");
            using var cursors = await collection.FindAsync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private async Task<Referee[]> GetReferees()
        {
            var collection = dataStore.GetCollection<Referee>("referee");
            using var cursors = await collection.FindAsync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }
        private async Task<Coach[]> GetCoaches()
        {
            var collection = dataStore.GetCollection<Coach>("coach");
            using var cursors = await collection.FindAsync(new BsonDocument());
            return cursors.ToList().Where(x => x != null).ToArray();
        }

        public LoginModel()
        {
            LoginViewModel = new LoginViewModel();
            Db = new MongoClient(Config.ConnectionString);
            dataStore = Db.GetDatabase("premierleague");
        }

        public LoginViewModel LoginViewModel { get; set; }

        protected async Task LoginAsync()
        {
            if (!(persons.Any() && players.Any() && coaches.Any() && referees.Any()))
            {
                persons = (await GetPersons()).ToList();
                players = (await GetPlayers()).ToList();
                referees = (await GetReferees()).ToList();
                coaches = (await GetCoaches()).ToList();
            }
            if (persons.All(x => x.Login != LoginViewModel.UserName) ||
                (persons.Any(x => x.Login == LoginViewModel.UserName) &&
                 !BCrypt.Net.BCrypt.Verify(LoginViewModel.Password, BCrypt.Net.BCrypt.HashPassword(LoginViewModel.Password))))
            {
                NavigationManager.NavigateTo("/login", true);
            }
            else
            {
                var token = new SecurityToken()
                {
                    AccessToken = LoginViewModel.Password,
                    UserName = LoginViewModel.UserName,
                    ExpiredAt = DateTime.UtcNow.AddDays(1)
                };
                await LocalStorageService.SetAsync(nameof(SecurityToken), token);
                Config.UserName = persons.First(x => x.Login == LoginViewModel.UserName).Name;

                NavigationManager.NavigateTo("/", true);
            }
        }
    }

    public class LoginViewModel
    {
        [Required]
        [StringLength(50, ErrorMessage = "Too long")]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }

    public class SecurityToken
    {
        public string UserName { get; set; }
        public string AccessToken { get; set; }
        public DateTime ExpiredAt { get; set; }
    }
}
