﻿using System.ComponentModel.DataAnnotations;
using BlazorRPL.Mappings;
using Microsoft.AspNetCore.Components;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorRPL.Pages
{
    public class PersonDisqualification : ComponentBase
    {
        public MongoClient Db { get; set; }
        private IMongoDatabase dataStore;
        public PersonDisqualificationViewModel PersonDisqualificationViewModel { get; set; }
        [Inject] public NavigationManager NavigationManager { get; set; }

        public PersonDisqualification()
        {
            PersonDisqualificationViewModel = new PersonDisqualificationViewModel();
            Db = new MongoClient(Config.ConnectionString);
            dataStore = Db.GetDatabase("premierleague");
        }

        public async Task RegisterDisqualificationAsync()
        {
            if (string.IsNullOrEmpty(PersonDisqualificationViewModel.Name) || 
                string.IsNullOrEmpty(PersonDisqualificationViewModel.Reason) ||
                PersonDisqualificationViewModel.From < new DateTime(2022, 1, 7) ||
                PersonDisqualificationViewModel.To < new DateTime(2022, 1, 7))
            {
                NavigationManager.NavigateTo("/Disqualificate");
                return;
            }

            var personCollection = dataStore.GetCollection<Person>("person");
            var disqualificationCollection = dataStore.GetCollection<DisqualificationVerdict>("disqualificationVerdict");
            using var cursors = await personCollection.FindAsync(new BsonDocument());
            var persons = cursors.ToList().Where(x => x != null).ToArray();
            var person = persons.First(x => x.Name == PersonDisqualificationViewModel.Name);
            var verdict = new DisqualificationVerdict()
            {
                PersonId = person.Id,
                Reason = PersonDisqualificationViewModel.Reason,
                DisqualifiedFrom = PersonDisqualificationViewModel.From,
                DisqualifiedTo = PersonDisqualificationViewModel.To
            };
            await disqualificationCollection.InsertOneAsync(verdict);
            NavigationManager.NavigateTo("/DisqualificationSuccessAdded");
        }
    }

    public class PersonDisqualificationViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Reason { get; set; }
        [Required]
        public DateTime From { get; set; }
        [Required]
        public DateTime To { get; set; }
    }
}
